﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public int enemyCurrentHealth;
    public int enemyMaxHealth;
    public GameObject itemDrop;

    private GameObject theEnemy;
    private GameObject thePlayer;
    
    //current stats for enemies. Can later be changed.
    void Start ()
    {
        enemyMaxHealth = 60;
        enemyCurrentHealth = enemyMaxHealth;
        theEnemy = this.gameObject;
        thePlayer = GameObject.FindWithTag("Player");
    }
	
	void Update ()
    {
        if (enemyCurrentHealth <= 0)
        {



            //this.gameObject.GetComponent<EnemyController>.
            //potentially drops something upon death
            GameObject instance = Instantiate (itemDrop, theEnemy.transform.position, Quaternion.identity);
            instance.SetActive(true);

            //theEnemy.SetActive(false);
            //add score to player for killing enemy
            thePlayer.GetComponent<PlayerHealth>().addScore(enemyMaxHealth);
            Destroy(theEnemy);
        }
    }

    public void DamageEnemy(int damage)
    {
        enemyCurrentHealth -= damage;
    }
}
