﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tesla : Weapon
{

    new void Start()
    {
        Debug.Log("called me");
        fireRate = 0.05f;
        wp_damage = 50;
        //gunPic = "Assets/Resources/DrillDrop.png";
        shotSpawn = this.gameObject.transform;
        //Debug.Log(gunPic);
        shot = Resources.Load("Prefab/Teslabulltes", typeof(GameObject)) as GameObject;
        shot.GetComponent<Destroy>().shooter = shotSpawn.tag;
        //shot = (GameObject)Resources.Load("Assets/Prefab/bullets.prefab", typeof(GameObject)); ;
        //sound = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        //sound.volume = 0.05f;
        //sound.clip = attack;
    }
    public override void Set(int ammo)
    {
        //attack = Resources.Load("Sounds/ray", typeof(AudioClip)) as AudioClip;
        Sprite newSprite = Resources.Load<Sprite>("DiamondRay");
        shotPic.GetComponent<SpriteRenderer>().sprite = newSprite;
        shot.SetActive(true);
        shot.GetComponent<Bullet>().speed = 5;
        ShotingPositionleft = -1.18f;
        ShotingPositionright = -0.5f;
        wp_damage = 50;
        fireRate = 0.05f;
        wp_ammo = ammo;
        //stopSound();

    }
    public override void Attack()
    {
        //Debug.Log("My ray called");
        Debug.Log(wp_damage);
        Vector3 a, b, rotateA, rotateB;
        Vector3 left;
        Vector3 right;
        left = new Vector3(ShotingPositionleft, 0, 0);
        right = new Vector3(ShotingPositionright, 0, 0);
        a = left;
        b = right;
        //rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        //rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), a.y * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) - a.x * -1 * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), 0);
        rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), b.y * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) - b.x * -1 * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), 0);


        nextFire = Time.time + fireRate;
        bulletIns = Instantiate(shot, rotateA + shotSpawn.position, shotSpawn.rotation);
        bulletIns.GetComponent<Destroy>().shooter = shotSpawn.tag;
        wp_ammo -= 2;
        if (wp_ammo < 0)
        {
            wp_ammo = 0;
        }
        /*
        if (sound.isPlaying == false)
        {
            //attack.LoadAudioData();
            //sound.clip = attack;
            //sound.Play();
        }
        */
    }
    public override void PlayerAttack()
    {
        //Debug.Log();
        if (Input.GetKeyDown(KeyCode.Space) && Time.time > nextFire && (wp_ammo > 0))
        {
            //Debug.Log("Called");
            Attack();
        }
    }
    public override int GetDamege()
    {

        return wp_damage;
    }
}
