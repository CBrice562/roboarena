﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Weapons_Boosts : MonoBehaviour {

    public int addHlth;   
    public string boost;            //what drop is it?
    public SpriteRenderer boostPic; //pic of item drop

    private int randomDrop;
    private int time;
    private string Gun;  

    void Start()
    {
        boostPic = gameObject.GetComponent<SpriteRenderer>();       
        boost = "";
        addHlth = 0;
        time = 0;

        if (boost == "") //if there is no drop, destroy this item
        {
            Destroy(gameObject); 
        }
    }
    void Update()
    {
        time++;
        if (time == 600) //drop dissapears after some time
        {
            Destroy(gameObject);
        }
    }

  
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            switch(boost)
            {
                case "health":
                    //if player just picked up health/shield, than the'll just stay that color longer
                    if (other.gameObject.GetComponent<SpriteRenderer>().color != Color.white)
                    {
                        other.gameObject.GetComponent<PlayerController>().hurt_time = 1;
                    }
                    other.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7382F, 0.5514F, 0.7641F);
                    other.gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7382F, 0.5514F, 0.7641F);
                    other.gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7382F, 0.5514F, 0.7641F);
                    other.gameObject.GetComponent<PlayerHealth>().addHealth(addHlth);                   
                    break;
                case "shield":
                    if (other.gameObject.GetComponent<SpriteRenderer>().color != Color.white)
                    {
                        other.gameObject.GetComponent<PlayerController>().hurt_time = 1;
                    }
                    other.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.8396F, 0.8198F, 0.8198F);
                    other.gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.8396F, 0.8198F, 0.8198F);
                    other.gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.8396F, 0.8198F, 0.8198F);
                    other.gameObject.GetComponent<PlayerHealth>().addShield(addHlth);
                    break;
                case "dropWeapon":
                    //currently, I have it so the player chooses to currently holds weapon it runs over. Will change soon.
                    other.gameObject.GetComponent<PlayerController>().setGun(999);
                    break;
                default:
                    break;
            }
            Destroy(gameObject);
        }
    }
}
