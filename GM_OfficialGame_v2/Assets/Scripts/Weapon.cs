﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Weapon : MonoBehaviour {

    public Transform shotSpawn;
    public GameObject shot;
    public GameObject shotPic;        //pic of the bullet
    public AudioSource sound;  //source of weapon sounds
    public AudioClip attack;          //weapon sound clip
    public float ShotingPositionleft = -1.9f;
    public float ShotingPositionright = -0.5f;
    public int wp_damage;
    public int wp_ammo = 300;
    public int wp_cap = 300;
    public Image weaponPic;
    public string gunPic;
    public string weaponname = "AssaultGun";
    //should be range around 0 -1000
    public int droprate = 500;
    public float fireRate =0.5f;

    protected float nextFire;
    protected GameObject bulletIns;

    public Weapon() {
    }

    void Start() {
        wp_damage = 20;
        //gunPic = "Assets/Resources/DrillDrop.png";
        shotSpawn = this.gameObject.transform;
        //Debug.Log(gunPic);
        shot = Resources.Load("Prefab/bullets", typeof(GameObject)) as GameObject;
        shot.GetComponent<Destroy>().shooter = shotSpawn.tag;
        //shot = (GameObject)Resources.Load("Assets/Prefab/bullets.prefab", typeof(GameObject)); ;
        //sound = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        //sound.volume = 0.05f;
        //sound.clip = attack;
    }

    public virtual int GetDamege() {

        return wp_damage;
    }
    public int GetAmmo()
    {
        return wp_ammo;
    }
    public virtual int setDropRate(int rate) {
        droprate = rate;
        return droprate;

    }

    public int getDropRate() {
        return droprate;
    }

    public virtual void Set(int ammo)
    {
        //attack = Resources.Load("Sounds/assault", typeof(AudioClip)) as AudioClip;
        Sprite newSprite = Resources.Load<Sprite>("DiamondAssault");

        //shotPic.GetComponent<SpriteRenderer>().sprite = newSprite;
        shot.GetComponent<Bullet>().speed = 10;
        ShotingPositionleft = -1.9f;
        ShotingPositionright = -0.5f;
        wp_damage = 20;
        nextFire = 0.9f;
        wp_ammo = ammo;

    }

    public string getName() {
        return name;
    }


    public virtual void PlayerAttack() {
        //Debug.Log();
        if (Input.GetKeyDown(KeyCode.Space) && Time.time > nextFire && (wp_ammo > 0))
        {
            //Debug.Log("Called");
            Attack();
        }
  }




    public virtual void Attack() {
        Debug.Log(wp_damage);
        Vector3 a, b, rotateA, rotateB;
        Vector3 left;
        Vector3 right;
        left = new Vector3(ShotingPositionleft, 0, 0);
        right = new Vector3(ShotingPositionright, 0, 0);
        a = left;
        b = right;
        //rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        //rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), a.y * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) - a.x * -1 * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), 0);
        rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), b.y * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) - b.x * -1 * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), 0);


        nextFire = Time.time + fireRate;
            bulletIns = Instantiate(shot, rotateA + shotSpawn.position, shotSpawn.rotation);
            bulletIns.GetComponent<Destroy>().shooter = shotSpawn.tag;
            bulletIns = Instantiate(shot, rotateB + shotSpawn.position, shotSpawn.rotation);
            bulletIns.GetComponent<Destroy>().shooter = shotSpawn.tag;
            wp_ammo -= 2;
            if (wp_ammo < 0)
            {
                wp_ammo = 0;
            }
            /*
            if (sound.isPlaying == false)
            {
                //attack.LoadAudioData();
                //sound.clip = attack;
                //sound.Play();
            }
            */
        }

}
