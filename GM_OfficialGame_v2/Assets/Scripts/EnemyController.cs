﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public int damageToPlayer; 
    public Transform target;
    public float speed = 3;
    public float hurt_time = 2; //hurt_time is for if the enemy is hit twice, they'll flash again only if finished flashing from the first hit.
    public Color oldColor;      //the original color of the bot
    public string weapon;       //weapon dropped by the enemy
    private Weapon wp;
    private float minDistance = 5;
    private float range;
    private EnemyHealth eHealth;    

    private void Start()
    {
        wp = random();
        target = GameObject.FindWithTag("Player").transform;
        eHealth = GetComponent<EnemyHealth>();
        oldColor = GetComponent<SpriteRenderer>().color;

        //coroutine = Delay(0.5f);
        //StartCoroutine(coroutine);
        //Debug.Log(wp);
        InvokeRepeating("attack", 0.5f,wp.fireRate);
    }
    //function to determine the weapon yeilded by the enemy. I made melee weapons just a little more likely than guns.
    //also, the weapon damages are the same for both the enemies and player.
    //Lastly, I made it so the player never picks up an empty gun. The random int goes from a low but not 0 num, to the max ammo.
    //The code so the enemies follow the player.
    void Update()
    {
        //rotation
        Vector3 vectorToTarget = target.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);

        transform.rotation = Quaternion.Slerp(transform.rotation , q, Time.deltaTime * speed);
        //end here
        range = Vector2.Distance(transform.position, target.position);

        hurt_time -= Time.deltaTime;
        if (hurt_time < 0)
        {
            GetComponent<SpriteRenderer>().color = oldColor;
            hurt_time = 2;
        }

        if (range > minDistance)
        {
            //Debug.Log(range);

            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
    }

    private void attack() {

        wp.Attack();

    }

    private Weapon random() {
        int rnd = Random.Range(1,4);
        switch(rnd){
            case 1:
                return GetComponent<MyRay>();
            case 2:
                return GetComponent<Weapon>();
            case 3:
                return GetComponent<Tesla>();
        }
        return GetComponent<Weapon>();
    }
}
