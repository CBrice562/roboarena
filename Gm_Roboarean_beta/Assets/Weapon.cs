﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
    float initial_damege = 10;
    public float fireRate;
    private float nextFire;
    public Transform shotSpawn;
    public GameObject shot;

    public void rangeAttack() {

        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        }

    }

}
