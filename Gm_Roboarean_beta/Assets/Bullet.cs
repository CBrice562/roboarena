﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject shotSpawn;
    Rigidbody2D rb2d;
    public float speed;
    void Start()
    {
       shotSpawn = GameObject.FindGameObjectsWithTag("Player")[0];
       rb2d = GetComponent<Rigidbody2D>();
       rb2d.AddRelativeForce(new Vector2(0,1)* speed, ForceMode2D.Impulse);
        //Debug.Log(shotSpawn.transform.forward);
        Destroy(gameObject,5);
    }

}
