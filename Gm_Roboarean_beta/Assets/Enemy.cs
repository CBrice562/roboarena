﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public Transform target;
    public float speed = 3;
    private float minDistance = 3;
    private float range;
    private void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
    }

    void Update()
    {  
        range = Vector2.Distance(transform.position, target.position);

        if (range > minDistance)
        {
            //Debug.Log(range);

            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            diff.Normalize();

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
    }
}
