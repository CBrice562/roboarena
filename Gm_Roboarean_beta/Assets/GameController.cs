﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class GameController : MonoBehaviour
{
    public GameObject hazard;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public int LIMIT = 10;
    public struct Gate
    {
        public Vector3 position;
        public Gate(float x, float y, float z) {
            position.x = x;
            position.y = y;
            position.z = z;
        }
    }
    Gate[] gates = new Gate[]{
        new Gate(-10, 8, 0),
        new Gate(10, 8, 0),
        new Gate(10, -8, 0),
        new Gate(-10, -8, 0)
    };

    void Start()
    {
        StartCoroutine(SpawnWaves());
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (GameObject.FindGameObjectsWithTag("Enemy").Length < LIMIT)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                int rd_num = Random.Range(0, 4);
                Vector3 spawnPosition = new Vector3(gates[rd_num].position.x, gates[rd_num].position.y, gates[rd_num].position.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
        }
    }
}