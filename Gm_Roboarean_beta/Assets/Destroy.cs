﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Hello");
        if (other.tag == "Enemy")
        {
            Destroy(gameObject);
            Destroy(other.gameObject);
        }
        else {
            return;
        }
    }

}
