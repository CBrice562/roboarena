﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingOb : MonoBehaviour {


    public float speed=20;             //Floating point variable to store the player's movement speed.

    private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.

    // Use this for initialization


    public float fireRate = 0.2f;
    private float nextFire;
    public Transform shotSpawn;
    public GameObject shot;
    public float ShotingPositionleft = -2.0f;
    public float ShotingPositionright = 0f;
    public void rangeAttack()
    {
        Vector3 a, b,rotateA,rotateB;
        Vector3 left = new Vector3(ShotingPositionleft, 0, 0);
        Vector3 right = new Vector3(ShotingPositionright,0, 0);
        a = shotSpawn.position + left;
        b = shotSpawn.position + right;

        rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.rotation.z) - a.y * Mathf.Sin(shotSpawn.rotation.z), a.y * Mathf.Cos(shotSpawn.rotation.z)  + a.x * Mathf.Sin(shotSpawn.rotation.z), 0);
        rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.rotation.z) - b.y * Mathf.Sin(shotSpawn.rotation.z), b.y * Mathf.Cos(shotSpawn.rotation.z) + b.x * Mathf.Sin(shotSpawn.rotation.z), 0);

        Debug.Log(rotateA.x+"  :  "+rotateA.y);
        if (Input.GetKey(KeyCode.Space) && Time.time > nextFire)
        {   
            nextFire = Time.time + fireRate;
            Instantiate(shot, rotateA, shotSpawn.rotation);
            Instantiate(shot, rotateB, shotSpawn.rotation);

        }

    }


    void Start()
    {

        //Get and store a reference to the Rigidbody2D component so that we can access it.
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.drag = 4;
        speed = 40;
    }

    //FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
    void Update()
    {

        rangeAttack();
        //wp.rangeAttack();
        //Store the current horizontal input in the float moveHorizontal.
        float moveHorizontal = Input.GetAxis("Horizontal");

        //Store the current vertical input in the float moveVertical.
        float moveVertical = Input.GetAxis("Vertical");

        //Use the two store floats to create a new Vector2 variable movement.
        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
        rb2d.AddForce(movement * speed);
    }
}
