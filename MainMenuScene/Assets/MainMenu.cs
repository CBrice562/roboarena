﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class MainMenu : MonoBehaviour {

	public void PlayGame()
    {
        //Placeholder name for next scene - Supposed to load up menu
        //SceneManager.LoadScene("RoboGame");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Debug.Log("Exiting Game");
        Application.Quit();
    }

    public void ControlsMenu()
    {
        //Placeholder name for next scene - Supposed to load up the controls menu
        //SceneManager.LoadScene("Controls Menu"); 
        //Assuming it wont be in the same scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
    }

    public void HighScoreMenu()
    {
        //Placeholder name for next scene - Supposed to load up the high score menu
        //SceneManager.LoadScene("High Score Menu"); 
        //Assuming it wont be in the same scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 3);
    }

}
