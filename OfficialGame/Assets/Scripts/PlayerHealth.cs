﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
    
    public Text healthText;
    public Text scoreText;
    public Text shieldText;
    public Text finalScore;
    public Text finalWave;
    public Text gameOver;
    public Button quit;
    public Button playAgain;
     
    private int playerHealth;
    private int playerShield;
    private int playerScore;
    private float waitToReload; //this float is just so there is a bit of a delay after death before respawning.
    private bool reloading;
    private GameObject thePlayer;
    private GameObject finalScoreObject;
    private GameObject finalWaveObject;
    private GameObject gameOverObject;
    private GameObject quitObject;
    private GameObject playObject;
    private GameObject gcontrol;

    void Start () {
        waitToReload = 2;
        playerHealth = 200 ;        
        playerShield = 200;
        setHealthText(playerHealth);
        setShieldText(playerShield);
        thePlayer = this.gameObject;
        finalScoreObject = GameObject.FindGameObjectWithTag("finalscore");
        finalScoreObject.SetActive(false);
        finalWaveObject = GameObject.FindGameObjectWithTag("finalround");
        finalWaveObject.SetActive(false);
        gameOverObject = GameObject.FindGameObjectWithTag("gameover");
        gameOverObject.SetActive(false);
        quitObject = GameObject.FindGameObjectWithTag("quitbtn");
        quitObject.SetActive(false);
        playObject = GameObject.FindGameObjectWithTag("playbtn");
        playObject.SetActive(false);
        gcontrol = GameObject.FindGameObjectWithTag("GameController"); 
    }
	
	void Update () {

        if (playerHealth <= 0)
        {
            Time.timeScale = 0;
            finalScoreObject.GetComponent<Text>().text = "Final Score: [ " + this.playerScore.ToString("0000000") + " ]";
            //finalWaveObject.GetComponent<Text>().text = "Final Wave: [ " + gcontrol.GetComponent<GameController>().getWave().ToString() + " ]";
            thePlayer.SetActive(false);
            finalScoreObject.SetActive(true);
            finalWaveObject.SetActive(true);
            gameOverObject.SetActive(true);
            quitObject.SetActive(true);
            playObject.SetActive(true);
        }
        //This code is a work in progress
        //if (reloading == true)
        //{
        //    waitToReload -= Time.deltaTime;
        //    if (waitToReload < 0)
        //    {
        //        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //        thePlayer.SetActive(true);
        //    }           
        //}
	}
    #region "UI Text Setting Methods"
    //I think the text setting methods are pretty self explanatory
    public void setHealthText(int newHealth)
    {
        if (newHealth > 0)
        {
            healthText.text = "Health: [ " + newHealth.ToString("000") + " ] 200";
        }
        else
        {
            healthText.text = "Powered Off";
        }
    }
    public void setShieldText(int newShield)
    {
        if (newShield > 0)
        {
            shieldText.text = "Shield: [ " + newShield.ToString("000") + " ] 200";
        }
        else 
        {
            shieldText.text = "Shield Down";
        }
    }
    public void setScoreText(int newScore)
    {
        scoreText.text = "Score: [ " + newScore.ToString("0000000") + " ]";       
    }
    #endregion
    #region "Player Health/Score Methods"
    public void DamagePlayer(int damage) 
    {
        if (playerShield > 0) //if theres a shield, damage the shield
        {
            playerShield -= damage;

            if (playerShield < 0) //shield/health should never go below 0.
            {                                
                playerShield = 0;
            }
            setShieldText(playerShield);            
        }
        else //shield is down
        {
            playerHealth -= damage;

            if (playerHealth < 0)
            {
                playerHealth = 0;               
            }
            setHealthText(playerHealth);
        }
    }
    public void addHealth(int hUp)
    {
        playerHealth += hUp;

        if (playerHealth > 200) //health/shield should not go over 200.
        {
            playerHealth = 200;
        }
        setHealthText(playerHealth);
    }
    public void addShield(int sUp)
    {
        playerShield += sUp;

        if (playerShield > 200)
        {
            playerShield = 200;           
        }
        setShieldText(playerShield);
    }
    public void addScore(int scoreUp)
    {
        playerScore += (scoreUp * 10); //score is accumulating via the health of the enemy * 10.

        if (playerScore > 9999999)
        {
            playerScore = 9999999;           
        }
        setScoreText(playerScore);
    }
    public int getScore()
    {
        return playerScore;
    }
    #endregion

    public void playAgainFunc()
    {
        //thePlayer.SetActive(true);
        Time.timeScale = 1;
        SceneManager.LoadScene("main");
    }
    public void quitGameFunc()
    {
        Application.Quit();
    }
}
