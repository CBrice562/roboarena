﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour {
     
    public Transform shotSpawn;       //the shot spawns from the player
    public GameObject shot;           //the bullet itself
    public int wp_damage;

    private float ShotingPositionleft = -1.18f;
    private float ShotingPositionright = 0f;    
    public float fireRate;
    private float nextFire;
    private Vector3 a, b, rotateA, rotateB;
    private Vector3 left;
    private Vector3 right;

    //#region "Misc Methods"
    public int getDamage()
    { 
        return wp_damage;
    }
    public void setAiming()
    {
        left = new Vector3(ShotingPositionleft, 0, 0);
        right = new Vector3(ShotingPositionright, 0, 0);
        a = left;
        b = right;
        //rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        //rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), a.y * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) - a.x * -1 * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), 0);
        rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), b.y * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) - b.x * -1 * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), 0);

        //    //if (Input.GetAxisRaw("Horizontal") == 0f && Input.GetAxisRaw("Vertical") < -0.5f)
        //    //{
        //    //    rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        //    //    rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        //    //}
    }
    //#endregion
    //#region "Set Weapon Methods"
    ////all of these set methods are just setting the settings for the weapon the player is currently using.
    ////each weapon has a different damage, fire rate, max ammo, etc 
    public void setAssaultGun()
    {
         shot = Resources.Load("Prefab/enAssault", typeof(GameObject)) as GameObject;
         //shot = GameObject.FindGameObjectWithTag("enAssault");
         shot.GetComponent<EnemyBullet>().speed = 6;
         ShotingPositionleft = -1.9f;
         ShotingPositionright = -0.5f;
         wp_damage = 10;
         fireRate = 0.8f;
    }
    public void setRayGun()
    {
        shot = Resources.Load("Prefab/enRay", typeof(GameObject)) as GameObject;
        //shot = GameObject.FindGameObjectWithTag("enRay");
        shot.GetComponent<EnemyBullet>().speed = 5;
        ShotingPositionleft = -1.18f;
        ShotingPositionright = -0f;
        wp_damage = 5;
        fireRate = 0.4f;
    }
    public void setTeslaCoil()
    {
        shot = Resources.Load("Prefab/enTesla", typeof(GameObject)) as GameObject;
        //shot = GameObject.FindGameObjectWithTag("enTesla");
        shot.GetComponent<EnemyBullet>().speed = 5;
        ShotingPositionleft = -1.18f;
        ShotingPositionright = -0f;
        wp_damage = 40;
        fireRate = 2.4f;
    }
    
    //#endregion
    //#region "Fire Weapon Methods"
    ////all of these methods are just the attacking methods of the weapons.
    public void fireAssaultGun()
    {
            if (Time.time > nextFire)
            {
                setAiming();
                nextFire = Time.time + fireRate;
                Instantiate(shot, shotSpawn.position + rotateA, shotSpawn.rotation);
                Instantiate(shot, shotSpawn.position + rotateB, shotSpawn.rotation);
            }
    }
    ////The ray gun can be fired via holding down the space bar
    public void fireRayGun()
    {
        if (Time.time > nextFire)
        {
            setAiming();
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position + rotateA, shotSpawn.rotation);
        }
    }
    public void fireTeslaCoil()
    {
            if (Time.time > nextFire)
            {
                setAiming();
                nextFire = Time.time + fireRate;
                Instantiate(shot, shotSpawn.position + rotateA, shotSpawn.rotation);               
            }
      
    }
   
    //#endregion
}
