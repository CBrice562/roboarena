﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Weapons_Boosts : MonoBehaviour {

    public int addHlth;   
    public string boost;            //what drop is it?
    public SpriteRenderer boostPic; //pic of item drop

    private int randomDrop;
    private int time;
    private int b_ammo;
    private string Gun;  

    void Start()
    {
        boostPic = gameObject.GetComponent<SpriteRenderer>();       
        boost = "";
        addHlth = 0;
        time = 0;
        //random int for choosing what is dropped.
        randomDrop = Random.Range(1, 101);
        ItemDrop(randomDrop);

        if (boost == "") //if there is no drop, destroy this item
        {
            Destroy(gameObject); 
        }
    }
    void Update()
    {
        time++;
        if (time == 600) //drop dissapears after some time
        {
            Destroy(gameObject);
        }
    }

    public void ItemDrop(int randomDrop)
    {
        if (randomDrop > 50 && randomDrop <= 65) //15% chance of health
        {
            boost = "health";
            addHlth = 30;
            setPic("healthCanister");
        }
        if (randomDrop > 65 && randomDrop <= 75) //10% chance of shield
        {
            boost = "shield";
            addHlth = 50;
            setPic("shieldCanister");
        }
        if (randomDrop > 75 && randomDrop <= 100) //25% chance enemy drops weapon
        {
            boost = "dropWeapon";
            addHlth = 0;           
        }
    }
    //this function gets what gun the enemy had, in case the enemy drops it.
    public void setGun(string weapon, string gunPic, int ammo)
    {
        Gun = weapon;
        b_ammo = ammo;
        setPic(gunPic);
    }
    //sets pic of item drop.
    public void setPic(string newBoostPic)
    {
        Sprite newSprite = Resources.Load<Sprite>(newBoostPic);
        boostPic.sprite = newSprite;
    }    
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            switch(boost)
            {
                case "health":
                    //if player just picked up health/shield, than the'll just stay that color longer
                    if (other.gameObject.GetComponent<SpriteRenderer>().color != Color.white)
                    {
                        other.gameObject.GetComponent<PlayerController>().hurt_time = 1;
                    }
                    other.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7382F, 0.5514F, 0.7641F);
                    other.gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7382F, 0.5514F, 0.7641F);
                    other.gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7382F, 0.5514F, 0.7641F);
                    other.gameObject.GetComponent<PlayerHealth>().addHealth(addHlth);                   
                    break;
                case "shield":
                    if (other.gameObject.GetComponent<SpriteRenderer>().color != Color.white)
                    {
                        other.gameObject.GetComponent<PlayerController>().hurt_time = 1;
                    }
                    other.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.8396F, 0.8198F, 0.8198F);
                    other.gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.8396F, 0.8198F, 0.8198F);
                    other.gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.8396F, 0.8198F, 0.8198F);
                    other.gameObject.GetComponent<PlayerHealth>().addShield(addHlth);
                    break;
                case "dropWeapon":
                    //currently, I have it so the player chooses to currently holds weapon it runs over. Will change soon.
                    other.gameObject.GetComponent<PlayerController>().setGun(Gun, b_ammo);
                    other.gameObject.GetComponent<Weapon>().stopSound();
                    break;
                default:
                    break;
            }
            Destroy(gameObject);
        }
    }
}
