﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;   
    public Text weaponTxt;
    public Text ammoText;
    public Transform head;  
    public Transform arms;   
    public AudioClip walk;   //robo walking sound
    public AudioClip action; //for sound when robo gets hit
    public AudioSource musicSource; //plays walking
    public AudioSource actionSource;//plays robo geting hit
    public float hurt_time = 1;     //tells how long robo changes color after being hurt (if he gets hit twice in a row, he stays red longer)
    public string pweapon;          //string to hold player current weapon
    public bool wasHit = true;
    public float mouseSpeed = 10f;
    public int ControllerType = 1;
    public InventoryManager InvManager;
    public GameObject InventoryPanel;

    private Quaternion rtate;
    private bool movingDown;
    private bool setDefault;
    private bool playerMoving;
    private string armSprite;
    private Vector2 lastMove;
    private Vector2 lastDir;
    private Animator anim;
    private Rigidbody2D myRigidbody;
    private PlayerHealth pHealth;   //player health script
    private Weapon wp;              //weapon script
      
    void Start() {
        
        anim = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody2D>();
        pHealth = GetComponent<PlayerHealth>();       
        wp = GetComponent<Weapon>();
        musicSource = GetComponent<AudioSource>();   
        head = this.gameObject.transform.GetChild(0);
        arms = this.gameObject.transform.GetChild(1);
        musicSource.clip = walk;
        actionSource.clip = action;
        setDefault = true;
        InvManager = GetComponent<InventoryManager>();
        InventoryPanel = GameObject.FindGameObjectWithTag("InventoryPanel");
        InventoryPanel.SetActive(false);
        pweapon = "boxing";
        setGun("boxing", 0);
    }
   
    void Update() {

        moveSpeed = 9;
        playerMoving = false;       
        hurt_time -= Time.deltaTime;
        wasHit = true;

        rangeAttack(); //shoot weapon

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (InventoryPanel.activeSelf == true)
            {
                InventoryPanel.SetActive(false);
                Time.timeScale = 1;
            }
            else
            {
                InventoryPanel.SetActive(true);
                Time.timeScale = 0;
            }
        }

        if (wp.getAmmo() <= 0 && setDefault == true)
        {
            pweapon = "boxing";
            setGun("boxing", 0);
            setDefault = false;
        }

        if (hurt_time < 0) //when robo finish being hurt, go back to normal color
        {
            GetComponent<SpriteRenderer>().color = Color.white;
            head.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            arms.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            hurt_time = 1;
        }
        //this if statement makes the speed moving diagonal equivalent to the speed moving regularly
        if (System.Math.Abs(Input.GetAxisRaw("Horizontal")) > 0.5f && System.Math.Abs(Input.GetAxisRaw("Vertical")) > 0.5f)
        {
            moveSpeed = 6;
        }
        //code to move player down
        if (System.Math.Abs(Input.GetAxisRaw("Horizontal")) == 0f && Input.GetAxisRaw("Vertical") < -0.5f)
        {
            rtate = Quaternion.Euler(0, 0, 180);
            movingDown = true;            
        }
        else if (System.Math.Abs(Input.GetAxisRaw("Horizontal")) > 0.5f || Input.GetAxisRaw("Vertical") > 0.5f)
        {
            movingDown = false;
        }
        //if (Input.GetAxisRaw("Horizontal") > 0.5f && Input.GetAxisRaw("Vertical") > 0.5f)
        //{
        //    rtate = Quaternion.Euler(0, 0, -45);
        //}
        
        lastMove = new Vector2(0f, 0f);
        //code to move player in other directions
        if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)
        {
            playerMoving = true;
            lastMove.x = Input.GetAxisRaw("Horizontal");         
        }
        if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f)
        {
            playerMoving = true;
            lastMove.y = Input.GetAxisRaw("Vertical");           
        }

        myRigidbody.velocity = new Vector2(lastMove.x*moveSpeed, lastMove.y*moveSpeed);
        
        anim.SetFloat("MoveX", Input.GetAxisRaw("Horizontal"));
        anim.SetFloat("MoveY", Input.GetAxisRaw("Vertical"));
        anim.SetBool("PlayerMoving", playerMoving);
        anim.SetFloat("LastMoveX", lastMove.x);
        anim.SetFloat("LastMoveY", lastMove.y);

        if (!playerMoving)
        {           
            if (movingDown == true)
            {
                transform.rotation = rtate;
            }
            else
            {
                transform.up = lastDir;
            }
        }
        else
        {
            if (musicSource.isPlaying == false) //robo walking will play only after current loop is done
            {
                musicSource.Play();
            }
            lastDir = myRigidbody.velocity;

            if (movingDown == true)
            {
                transform.rotation = rtate;               
            }
            else
            {
                transform.up = lastDir;
            }
        }               
    }
    //this function will be called whenever player chooses a new weapon to currently yeild.
    //the preferences and settings will be set in the weapon script. 
    //string weapon determines the weapon and ammo is just to keep track of how much that weapon has.
    public void setGun(string weapon, int ammo)
    {
        if (weapon == "assault")
        {
            wp.setAssaultGun(ammo);
            weaponTxt.text = "Mini Guns";
            ammoText.text = "Ammo: [ " + wp.getAmmo().ToString("000") + " ] 300";
            armSprite = "AssaultArms";
        }
        else if (weapon == "ray")
        {
            wp.setRayGun(ammo);
            weaponTxt.text = "Ray Gun";        
            ammoText.text = "Power: [ " + wp.getAmmo().ToString("000") + " ] 250";
            armSprite = "RayArms";
        }
        else if (weapon == "tesla")
        {
            wp.setTeslaCoil(ammo);
            weaponTxt.text = "Tesla Coil";           
            ammoText.text = "Teslas: [ " + wp.getAmmo().ToString("000") + " ] 130";
            armSprite = "TeslaArms";
        }
        else if (weapon == "stick")
        {
            wp.setDeathStick(ammo);
            weaponTxt.text = "Death Sticks";            
            ammoText.text = "Power: [ " + wp.getAmmo().ToString("000") + " ] 310";
            armSprite = "StickArms";
        }
        else if (weapon == "drill")
        {
            wp.setDrill(ammo);
            weaponTxt.text = "Drills";        
            ammoText.text = "Fuel: [ " + wp.getAmmo().ToString("000") + " ] 450";
            armSprite = "DrillArms";
        }
        else if (weapon == "boxing")
        {
            wp.setBoxing();
            weaponTxt.text = "Boxing";         
            ammoText.text = "";
            armSprite = "BoxingArms";
        }
        Sprite newSprite = Resources.Load<Sprite>(armSprite);
        arms.GetComponent<SpriteRenderer>().sprite = newSprite;
        setDefault = true;
        pweapon = weapon;
    }
    //this function is an active weapon attack
    public void rangeAttack()
    {
        if (pweapon == "assault")
        {
            wp.fireAssaultGun();
            ammoText.text = "Ammo: [ " + wp.getAmmo().ToString("000") + " ] 300";
        }
        else if (pweapon == "ray")
        {
            wp.fireRayGun();
            ammoText.text = "Power: [ " + wp.getAmmo().ToString("000") + " ] 250";
        }
        else if (pweapon == "tesla")
        {
            wp.fireTeslaCoil();
            ammoText.text = "Teslas: [ " + wp.getAmmo().ToString("000") + " ] 130";         
        }
        else if (pweapon == "stick")
        {
            wp.fireDeathStick();
            ammoText.text = "Power: [ " + wp.getAmmo().ToString("000") + " ] 310";
        }
        else if (pweapon == "drill")
        {
            wp.fireDrill();
            ammoText.text = "Fuel: [ " + wp.getAmmo().ToString("000") + " ] 450";
        }
        else if (pweapon == "boxing")
        {
            wp.fireBoxing();
        }        
    }
    public void makeHurt()
    {
        //for sound of player getting hurt
        action = Resources.Load("Sounds/robotHurt", typeof(AudioClip)) as AudioClip;
        action.LoadAudioData();
        actionSource.clip = action;
        if (GetComponent<SpriteRenderer>().color != Color.white)
        {
            hurt_time = 1;
        }
        actionSource.Play();
        //make color of bot hurt
        GetComponent<SpriteRenderer>().color = new Color(0.9905F, 0.7522F, 0.7522F);
        head.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.9905F, 0.7522F, 0.7522F);
        arms.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.9905F, 0.7522F, 0.7522F);
    }
    //This function checks if an enemy is hitting the player. Will be edited to check if bullets or melee are hitting the player.
    void OnCollisionEnter2D(Collision2D other)
    {
        if ((other.gameObject.tag == "Enemy" || other.gameObject.tag == "Enemy1") && wasHit == true)
        {
            makeHurt();
            //use the health script to take health from the player
            if (other.gameObject.tag == "Enemy")
            {
                pHealth.DamagePlayer(other.gameObject.GetComponent<EnemyController>().damageToPlayer);
            }
            else 
            {
                pHealth.DamagePlayer(other.gameObject.GetComponent<EnemyControllerShooter>().damageToPlayer);
            }
        }       
    }   
}
