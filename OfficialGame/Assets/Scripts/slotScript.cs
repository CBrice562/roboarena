﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class slotScript : MonoBehaviour {

    //public string SlotItem { get SlotItem; set SlotItem; }
    public string SlotItem;
    public int ItemAmmo;
    public GameObject player;

    void start()
    {
        this.SlotItem = "";
        this.ItemAmmo = 0;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void setItem(string item)
    {
        this.SlotItem = item;
    }

    public void setAmmo(int ammo)
    {
        Debug.Log("set ammo called");
        this.ItemAmmo = checkAmmo(ammo);
        Debug.Log(this.ItemAmmo.ToString());
    }

    public void setAmmoLoss(int ammo)
    {
        this.ItemAmmo = ammo;
    }

    public int getAmmo()
    {
        return this.ItemAmmo;
    }

    public string getItem()
    {
        return this.SlotItem;
    }

    private int checkAmmo(int ammo)
    {
        int newAmmo = this.ItemAmmo;
        Debug.Log("check ammo called");
        Debug.Log(this.SlotItem);
        Debug.Log(this.ItemAmmo.ToString());

        if (this.SlotItem == "assault" && this.ItemAmmo < 300)
        {
            newAmmo = this.ItemAmmo + ammo;
            if (newAmmo >= 300)
            {
                newAmmo = 300;
            }
            return newAmmo; 
        }
        else if (this.SlotItem == "ray" && this.ItemAmmo < 250)
        {
            newAmmo = this.ItemAmmo + ammo;
            if (newAmmo >= 250)
            {
                newAmmo = 250;
            }
            return newAmmo;
        }
        else if (this.SlotItem == "tesla" && this.ItemAmmo < 130)
        {
            newAmmo = this.ItemAmmo + ammo;
            if (newAmmo >= 130)
            {
                newAmmo = 130;
            }
            return newAmmo;
        }
        else if (this.SlotItem == "drill" && this.ItemAmmo < 450)
        {
            newAmmo = this.ItemAmmo + ammo;
            if (newAmmo >= 450)
            {
                newAmmo = 450;
            }
            return newAmmo;
        }
        else if (this.SlotItem == "stick" && this.ItemAmmo < 310)
        {
            newAmmo = this.ItemAmmo + ammo;
            if (newAmmo >= 310)
            {
                newAmmo = 310;
            }
            return newAmmo;
        }
        else
        {
            return newAmmo;
        }
    }
    public void OnMouseDown()
    {
        Debug.Log("On Mouse Down called");
        player.GetComponent<PlayerController>().setGun(this.SlotItem, this.ItemAmmo);
    }

}
