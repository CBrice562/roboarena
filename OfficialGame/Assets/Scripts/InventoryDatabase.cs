﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;
using UnityEngine.UI; 

public class InventoryDatabase : MonoBehaviour {

    //Creates database of items
    private List<Item> db = new List<Item>();
    //Creates Json reader object
    private JsonData itemData;

 
    private void Start()
    {
        //Reads data from Items.json
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/weapons.json"));
        ConstructItemDataBase();

    }


    public Item FetchItemById(int idnum)
    {
        Debug.Log("fetched item");
        Debug.Log(db[2].Title);
        foreach (Item s in db)
        {
            if (s.ID == idnum)
            {
                Debug.Log("Fetched " + s.Title); 
                return s; 
            } 
        }
        return null; 
    }

    void ConstructItemDataBase()
    {
        //Constructs database
        foreach (JsonData j in itemData)
        {
            //Needs to be updated for integration
            db.Add(new Item((int)j["id"], (string)j["title"], (int)j["num"])); 
        }

    }
}

public class Item
{
    //Alternate Constructor
    public Item(int id, string title, int num)
    {
        this.ID = id;
        this.Title = title;
        this.Value = num; 
    }

    //Default Constructor
    public Item()
    {
        this.ID = -1;
        Debug.Log("Something's wrong"); 
    }

    //Simple getters/setters for inventory
    //We will add more stats, power, damage, bullets, chargetime, legs, chaisse etc as we go
    public int ID { get; set; }
    public string Title { get; set; }
    public int Value { get; set;  }
    //From this point on, none of the getters/setters will be implemented by the constructor until finalization (SHOULD NOT BE LEFT IN AFTER INTEGRATION)
    public bool RangeorMelee { get; set; }
    public int Damage { get; set; }
    public int Ammo { get; set; }
    public string Description { get; set; }
    public Sprite Sprite { get; set; }
   
}
