﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
     
    public Transform shotSpawn;       //the shot spawns from the player
    public GameObject shot;           //the bullet itself
    public GameObject shotPic;        //pic of the bullet
    public AudioSource actionSource;  //source of weapon sounds
    public AudioClip attack;          //weapon sound clip 
    public int wp_damage;
    public int wp_ammo;
    public int wp_ID;

    private float ShotingPositionleft = -1.18f;
    private float ShotingPositionright = 0f;    
    private float fireRate;
    private float nextFire;
    private Vector3 a, b, rotateA, rotateB;
    private Vector3 left;
    private Vector3 right;

    void Start()
    {
        //setBoxing();        
        actionSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        actionSource.volume = 0.05f;       
        actionSource.clip = attack;
    }
    #region "Misc Methods"
    public int getAmmo()
    { 
        return wp_ammo;
    }
    public void stopSound()
    {
        actionSource.Stop();
    }
    public void playSound()
    {
        if (actionSource.isPlaying == false)
        {
            attack.LoadAudioData();
            actionSource.clip = attack;
            actionSource.Play();
        }
    }
    public void setSlotAmmo()
    {
        shotSpawn.GetComponent<InventoryManager>().setSlotAmmo(shotSpawn.GetComponent<PlayerController>().pweapon, wp_ammo);
    }
    public void setAiming()
    {
        left = new Vector3(ShotingPositionleft, 0, 0);
        right = new Vector3(ShotingPositionright, 0, 0);
        a = left;
        b = right;
        //rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        //rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), a.y * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) - a.x * -1 * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), 0);
        rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), b.y * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) - b.x * -1 * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), 0);
        
        //if (Input.GetAxisRaw("Horizontal") == 0f && Input.GetAxisRaw("Vertical") < -0.5f)
        //{
        //    rotateA = new Vector3(a.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + a.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        //    rotateB = new Vector3(b.x * Mathf.Cos(shotSpawn.eulerAngles.z * Mathf.Deg2Rad) + b.y * Mathf.Sin(shotSpawn.eulerAngles.z * Mathf.Deg2Rad), -180, 0);
        //}
    }
    #endregion
    #region "Set Weapon Methods"
    //all of these set methods are just setting the settings for the weapon the player is currently using.
    //each weapon has a different damage, fire rate, max ammo, etc 
    public void setAssaultGun(int ammo)
    {
        wp_ID = 6;
        attack = Resources.Load("Sounds/assault", typeof(AudioClip)) as AudioClip;       
        Sprite newSprite = Resources.Load<Sprite>("DiamondAssault");
        shotPic.GetComponent<SpriteRenderer>().sprite = newSprite;
        shot.SetActive(true);
        shot.GetComponent<Bullet>().speed = 10;
        ShotingPositionleft = -1.9f;
        ShotingPositionright = -0.5f;
        wp_damage = 10;
        fireRate = 0.2f;
        wp_ammo = ammo;
        stopSound();
    }
    public void setRayGun(int ammo)
    {
        wp_ID = 4;
        attack = Resources.Load("Sounds/ray", typeof(AudioClip)) as AudioClip;
        Sprite newSprite = Resources.Load<Sprite>("DiamondRay");
        shotPic.GetComponent<SpriteRenderer>().sprite = newSprite;
        shot.SetActive(true);
        shot.GetComponent<Bullet>().speed = 5;
        ShotingPositionleft = -1.18f;
        ShotingPositionright = -0f;
        wp_damage = 5;
        fireRate = 0.1f;
        wp_ammo = ammo;
        stopSound();
    }
    public void setTeslaCoil(int ammo)
    {
        wp_ID = 5;
        attack = Resources.Load("Sounds/tesla", typeof(AudioClip)) as AudioClip;
        Sprite newSprite = Resources.Load<Sprite>("DiamondTesla");
        shotPic.GetComponent<SpriteRenderer>().sprite = newSprite;
        shot.SetActive(true);
        shot.GetComponent<Bullet>().speed = 8;
        ShotingPositionleft = -1.18f;
        ShotingPositionright = -0f;
        wp_damage = 40;
        fireRate = 0.6f;
        wp_ammo = ammo;
        stopSound();
    }
    public void setDeathStick(int ammo)
    {
        wp_ID = 3;
        attack = Resources.Load("Sounds/stick", typeof(AudioClip)) as AudioClip;
        shot.SetActive(false);
        fireRate = 0f;
        wp_damage = 50;
        wp_ammo = ammo;
        stopSound();
    }
    public void setDrill(int ammo)
    {
        wp_ID = 2;
        attack = Resources.Load("Sounds/drill", typeof(AudioClip)) as AudioClip;
        shot.SetActive(false);
        fireRate = 0.9f;
        wp_damage = 30;
        wp_ammo = ammo;
        stopSound();
    }
    public void setBoxing()
    {
        wp_ID = 1;
        shot.SetActive(false);
        fireRate = 0f;
        wp_damage = 15;       
        stopSound();
    }
    #endregion
    #region "Fire Weapon Methods"
    //all of these methods are just the attacking methods of the weapons.
    public void fireAssaultGun()
    {
        if (Input.GetKey(KeyCode.X) && (wp_ammo > 0))
        {
            if (Time.time > nextFire)
            {
                setAiming();
                nextFire = Time.time + fireRate;
                Instantiate(shot, shotSpawn.position + rotateA, shotSpawn.rotation);
                Instantiate(shot, shotSpawn.position + rotateB, shotSpawn.rotation);              
                wp_ammo -= 2;
                setSlotAmmo();

                if (wp_ammo < 0)
                {
                    wp_ammo = 0;
                }
            }
            playSound();
        }
        else
        {
            stopSound();
        }
    }
    //The ray gun can be fired via holding down the space bar
    public void fireRayGun()
    {        
        if (Input.GetKey(KeyCode.X) && (wp_ammo > 0))
        {
            if (Time.time > nextFire)
            {
                setAiming();
                nextFire = Time.time + fireRate;
                Instantiate(shot, shotSpawn.position + rotateA, shotSpawn.rotation);
                wp_ammo -= 1;
                setSlotAmmo();
            }
            playSound();
        }
        else
        {
            stopSound();
        }
    }
    public void fireTeslaCoil()
    {
        if (Input.GetKeyDown(KeyCode.X) && Time.time > nextFire && (wp_ammo > 0))
        {
            setAiming();
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position + rotateA, shotSpawn.rotation);
            wp_ammo -= 1;
            setSlotAmmo();

            attack.LoadAudioData();
            actionSource.clip = attack;
            actionSource.Play();
        }
    }
    public bool fireDeathStick()
    {
        bool Ehit = false;
        if (Input.GetKey(KeyCode.X) && (wp_ammo > 0))
        {
            wp_ammo -= 1;
            playSound();
            Ehit = true;
            setSlotAmmo();
        }
        else
        {
            stopSound();
        }

        return Ehit;
    }
    public bool fireDrill()
    {
        bool Ehit = false;
        if (Input.GetKey(KeyCode.X) && (wp_ammo > 0))
        {
            wp_ammo -= 1;
            playSound();
            Ehit = true;
            setSlotAmmo();
        }
        else
        {
            stopSound();
        }
        return Ehit;
    }
    public bool fireBoxing()
    {
        bool Ehit = false;
        if (Input.GetKey(KeyCode.X))
        {
            Ehit = true;
        }
        return Ehit;
    }
    #endregion
}
