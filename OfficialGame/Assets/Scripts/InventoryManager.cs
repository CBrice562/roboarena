﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour {

    GameObject InventoryPanel;
    GameObject PicturePanel;
    public GameObject InventorySlot;
    public GameObject itemObj;
    public Image slot1;
    public Image slot2;
    public Image slot3;
    public Image slot4;
    public Image slot5;
    public Image slot6;
    public Sprite s1; 

    InventoryDatabase db; 

    public List<Item> Items = new List<Item>();
    public List<GameObject> slots = new List<GameObject>();

    int slotmax;
    //1 boxing, 2 drill, 3 deathstick, 4 ray, 5 tesla, 6 minigun 


    void Start()
    {
        db = this.gameObject.GetComponent<InventoryDatabase>();
        InventoryPanel = GameObject.Find("InventoryPanel");
        //PicturePanel = InventoryPanel.transform.Find("PicturePanel").gameObject;

        //slot1 = GameObject.FindGameObjectWithTag("slot1");
        //slot2 = GameObject.FindGameObjectWithTag("slot2");
        //slot3 = GameObject.FindGameObjectWithTag("slot3");
        //slot4 = GameObject.FindGameObjectWithTag("slot4");
        //slot5 = GameObject.FindGameObjectWithTag("slot5");
        //slot6 = GameObject.FindGameObjectWithTag("slot6");
        
        //slotmax = 6; 
        //InventoryPanel = GameObject.Find("InventoryPanel");
        //PicturePanel = InventoryPanel.transform.Find("PicturePanel").gameObject;
 
        //for (int i = 0; i < slotmax; i++)
        //{
        //    Items.Add(new Item());
        //    slots.Add(Instantiate(InventorySlot));
       
        //}
        
        //Items.Add(new Item());
        //AddItem(0, 0);        
    }
    public Sprite setPic(string weapon)
    {
        if (weapon == "assault")
        {
            return Resources.Load<Sprite>("AssaultDrop");
        }
        else if (weapon == "ray")
        {
            return Resources.Load<Sprite>("RaygunDrop");
        }
        else if (weapon == "tesla")
        {
            return Resources.Load<Sprite>("TeslaCoilDrop");
        }
        else if (weapon == "stick")
        {
            return Resources.Load<Sprite>("DeathStickDrop");
        }
        else if (weapon == "drill")
        {
            return Resources.Load<Sprite>("DrillDrop");
        }
        else
        {
            return Resources.Load<Sprite>("BoxingDrop");
        }
    }
    public void AddItem(int idnum, int ammo)
    {
        Item newItem = db.FetchItemById(idnum);

        Debug.Log(idnum.ToString());
        Debug.Log(newItem.Title);
        Debug.Log(slot1.GetComponent<slotScript>().getItem());
    
        bool alreadyHave = (slot1.GetComponent<slotScript>().getItem() == newItem.Title || slot2.GetComponent<slotScript>().getItem() == newItem.Title
            || slot3.GetComponent<slotScript>().getItem() == newItem.Title || slot4.GetComponent<slotScript>().getItem() == newItem.Title
            || slot5.GetComponent<slotScript>().getItem() == newItem.Title || slot6.GetComponent<slotScript>().getItem() == newItem.Title);

        if (Items.Count >= 6)
        {
            //Do nothing;  
        }
        else
        {
            if (alreadyHave == true)
            {
                if (slot1.GetComponent<slotScript>().getItem() == newItem.Title)
                {
                    slot1.GetComponent<slotScript>().setAmmo(ammo);
                    this.gameObject.GetComponent<Weapon>().wp_ammo = slot1.GetComponent<slotScript>().getAmmo();
                }
                else if (slot2.GetComponent<slotScript>().getItem() == newItem.Title)
                {
                    slot2.GetComponent<slotScript>().setAmmo(ammo);
                    this.gameObject.GetComponent<Weapon>().wp_ammo = slot2.GetComponent<slotScript>().getAmmo();
                }
                else if (slot3.GetComponent<slotScript>().getItem() == newItem.Title)
                {
                    slot3.GetComponent<slotScript>().setAmmo(ammo);
                    this.gameObject.GetComponent<Weapon>().wp_ammo = slot3.GetComponent<slotScript>().getAmmo();
                }
                else if (slot4.GetComponent<slotScript>().getItem() == newItem.Title)
                {
                    slot4.GetComponent<slotScript>().setAmmo(ammo);
                    this.gameObject.GetComponent<Weapon>().wp_ammo = slot4.GetComponent<slotScript>().getAmmo();
                }
                else if (slot5.GetComponent<slotScript>().getItem() == newItem.Title)
                {
                    slot5.GetComponent<slotScript>().setAmmo(ammo);
                    this.gameObject.GetComponent<Weapon>().wp_ammo = slot5.GetComponent<slotScript>().getAmmo();
                }
                else if (slot6.GetComponent<slotScript>().getItem() == newItem.Title)
                {
                    slot6.GetComponent<slotScript>().setAmmo(ammo);
                    this.gameObject.GetComponent<Weapon>().wp_ammo = slot6.GetComponent<slotScript>().getAmmo();
                }
            }
            
            if (slot1.GetComponent<slotScript>().getItem() == "" && alreadyHave == false)
            {               
                if (newItem == null)
                {
                    //Keep it blank
                }
                else
                {
                    slot1.GetComponent<slotScript>().setItem(newItem.Title);
                    slot1.GetComponent<slotScript>().setAmmo(ammo);
                    slot1.GetComponent<Image>().sprite = setPic(newItem.Title);
                    //s1.sprite = ph.Sprite; 
                }
                Items.Add(newItem);
            }
            else if (slot2.GetComponent<slotScript>().SlotItem == "" && alreadyHave == false)
            {                
                if (newItem == null)
                {
                    //Keep it blank
                }
                else
                {
                    slot2.GetComponent<slotScript>().SlotItem = newItem.Title;
                    slot2.GetComponent<slotScript>().setAmmo(ammo);
                    slot2.GetComponent<Image>().sprite = setPic(newItem.Title);
                    //s1.sprite = ph.Sprite; 
                }
                Items.Add(newItem);
            }
            else if (slot3.GetComponent<slotScript>().SlotItem == "" && alreadyHave == false)
            {               
                if (newItem == null)
                {
                    //Keep it blank
                }
                else
                {
                    slot3.GetComponent<slotScript>().SlotItem = newItem.Title;
                    slot3.GetComponent<slotScript>().setAmmo(ammo);
                    slot3.GetComponent<Image>().sprite = setPic(newItem.Title);
                    //s1.sprite = ph.Sprite; 
                }
                Items.Add(newItem);
            }
            else if (slot4.GetComponent<slotScript>().SlotItem == "" && alreadyHave == false)
            {                
                if (newItem == null)
                {
                    //Keep it blank
                }
                else
                {
                    slot4.GetComponent<slotScript>().SlotItem = newItem.Title;
                    slot4.GetComponent<slotScript>().setAmmo(ammo);
                    slot4.GetComponent<Image>().sprite = setPic(newItem.Title);
                    //s1.sprite = ph.Sprite; 
                }
                Items.Add(newItem);
            }
            else if (slot5.GetComponent<slotScript>().SlotItem == "" && alreadyHave == false)
            {               
                if (newItem == null)
                {
                    //Keep it blank
                }
                else
                {
                    slot5.GetComponent<slotScript>().SlotItem = newItem.Title;
                    slot5.GetComponent<slotScript>().setAmmo(ammo);
                    slot5.GetComponent<Image>().sprite = setPic(newItem.Title);
                    //s1.sprite = ph.Sprite; 
                }
                Items.Add(newItem);
            }
            else if (slot6.GetComponent<slotScript>().SlotItem == "" && alreadyHave == false)
            {                
                if (newItem == null)
                {
                    //Keep it blank
                }
                else
                {
                    slot6.GetComponent<slotScript>().SlotItem = newItem.Title;
                    slot6.GetComponent<slotScript>().setAmmo(ammo);
                    slot6.GetComponent<Image>().sprite = setPic(newItem.Title);
                    //s1.sprite = ph.Sprite; 
                }
                Items.Add(newItem);
            }
        }        
        //for(int i = 0; i < Items.Count; i++)
        //{
            //if(Items[i].ID == -1)
            //{
            //    //No item, instantiate a blank picture
            //    /*
            //    Items[i] = newItem;
            //    itemObj = Instantiate(InventoryItem);
            //    itemObj.transform.SetParent(slots[i].transform);
            //    itemObj.transform.position = Vector2.zero;
            //    */
            //}
        //}
    }
    public void setSlotAmmo(string currentWp, int ammo)
    {
        if (slot1.GetComponent<slotScript>().getItem() == currentWp)
        {
            slot1.GetComponent<slotScript>().setAmmoLoss(ammo);
        }
        else if (slot2.GetComponent<slotScript>().getItem() == currentWp)
        {
            slot2.GetComponent<slotScript>().setAmmoLoss(ammo);
        }
        else if (slot3.GetComponent<slotScript>().getItem() == currentWp)
        {
            slot3.GetComponent<slotScript>().setAmmoLoss(ammo);
        }
        else if (slot4.GetComponent<slotScript>().getItem() == currentWp)
        {
            slot4.GetComponent<slotScript>().setAmmoLoss(ammo);
        }
        else if (slot5.GetComponent<slotScript>().getItem() == currentWp)
        {
            slot5.GetComponent<slotScript>().setAmmoLoss(ammo);
        }

    }
}
