﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {
    
    public GameObject itemDrop;

    private GameObject theEnemy;
    private GameObject thePlayer;
    private int enemyCurrentHealth;
    private int enemyMaxHealth;

    //current stats for enemies. Can later be changed.
    void Start ()
    {
        theEnemy = this.gameObject;
        if (theEnemy.tag == "Enemy")
        {
            enemyMaxHealth = 60;
        }
        else
        {
            enemyMaxHealth = 30;
        }
        
        enemyCurrentHealth = enemyMaxHealth;     
        thePlayer = GameObject.FindWithTag("Player");
    }
	
	void Update ()
    {
        if (enemyCurrentHealth <= 0)
        {
            //potentially drops something upon death
            GameObject instance = Instantiate (itemDrop, theEnemy.transform.position, Quaternion.identity);
            instance.SetActive(true);

            if (this.gameObject.tag == "Enemy")
            {
                instance.GetComponent<itemDrop>().setWeaponDrop(theEnemy.GetComponent<EnemyController>().weapon, theEnemy.GetComponent<EnemyController>().gunPic, theEnemy.GetComponent<EnemyController>().wp_ammo);
            }
            else
            {
                instance.GetComponent<itemDrop>().setWeaponDrop(theEnemy.GetComponent<EnemyControllerShooter>().weapon, theEnemy.GetComponent<EnemyControllerShooter>().gunPic, theEnemy.GetComponent<EnemyControllerShooter>().wp_ammo);
            }

            Destroy(theEnemy);
            //add score to player for killing enemy
            thePlayer.GetComponent<PlayerHealth>().addScore(enemyMaxHealth);
        }
    }

    public void DamageEnemy(int damage)
    {
        enemyCurrentHealth -= damage;
    }
}
