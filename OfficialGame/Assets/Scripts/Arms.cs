﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arms : MonoBehaviour {

    public PlayerController playerCon;
    public Weapon playerWP;
    public bool Ehit;
    private int knockback;

    void Start()
    {
        playerCon = gameObject.GetComponentInParent<PlayerController>();
        playerWP = gameObject.GetComponentInParent<Weapon>();
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Enemy1")
        {           
            if (playerCon.pweapon == "stick")
            {
                Ehit = this.gameObject.GetComponentInParent<Weapon>().fireDeathStick();
                playerCon.ammoText.text = "Power: [ " + playerWP.getAmmo().ToString("000") + " ] 310";
                knockback = 8;
            }
            else if (playerCon.pweapon == "drill")
            {
                Ehit = this.gameObject.GetComponentInParent<Weapon>().fireDrill();
                playerCon.ammoText.text = "Fuel: [ " + playerWP.getAmmo().ToString("000") + " ] 450";
                knockback = 5;
            }
            else if (playerCon.pweapon == "boxing")
            {
                Ehit = this.gameObject.GetComponentInParent<Weapon>().fireBoxing();
                knockback = 3;
            }
            if (Ehit == true) //damaged enemy
            {
                playerCon.wasHit = false;
                Ehit = false;
                //make enemy flash yellow when hit
                if (other.gameObject.tag == "Enemy")
                {
                    if (other.gameObject.GetComponent<SpriteRenderer>().color != other.gameObject.GetComponent<EnemyController>().oldColor)
                    {
                        other.gameObject.GetComponent<EnemyController>().setHurtTime(2);
                    }
                    other.gameObject.GetComponent<EnemyController>().setMinDistance(knockback);
                }
                else
                {
                    if (other.gameObject.GetComponent<SpriteRenderer>().color != other.gameObject.GetComponent<EnemyControllerShooter>().oldColor)
                    {
                        other.gameObject.GetComponent<EnemyControllerShooter>().setHurtTime(2);
                    }
                    other.gameObject.GetComponent<EnemyControllerShooter>().setMinDistance(knockback);
                }

                other.gameObject.GetComponent<SpriteRenderer>().color = Color.yellow;
                //damage player based on the weapon damage of the weapon used by the player
                other.gameObject.GetComponent<EnemyHealth>().DamageEnemy(GameObject.FindWithTag("Player").GetComponent<Weapon>().wp_damage);               
            }
        }
    }
}
