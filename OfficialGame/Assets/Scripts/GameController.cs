﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject hazard1;
    public GameObject hazard2;
    public int hazardCount;
    public int hazardCount2;
    public int h1lim;
    public int h2lim;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public int LIMIT = 1;
    public Text waves;
    public int waveNum;
    public int current;
    public bool inif = true;
    public struct Gate
    {
        public Vector3 position;
        public Gate(float x, float y, float z)
        {
            position.x = x;
            position.y = y;
            position.z = z;
        }
    }
    Gate[] gates1 = new Gate[]{
        new Gate(-11, 3, 0),
        new Gate(11.9f, 5.7f, 0),
        new Gate(3.1f, -9.6f, 0)
    };
    Gate[] gates2 = new Gate[]{
        new Gate(-11, 3, 0),
        new Gate(11.9f, 5.7f, 0),
        new Gate(3.1f, -9.6f, 0)
    };
    //I believe the above Gates are spawning coordinates for the enemies. I can't say much though since guanqing wrote this portion.
    void Start()
    {       
        //LIMIT = 1;
        waveNum = 1;
        waves.text = "Wave: [ 1 ]";
        hazardCount = 4;
        //StartCoroutine(Waves());
        StartCoroutine(SpawnWaves());
        //hazardCount1 = 2;
        //hazardCount2 = 1;
        //h1lim = 0;
        //h2lim = 0;
    }
    public int getWave()
    {
        return waveNum;
    }

    void wiatSeconds()
    {
        while (true)
        {
            if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 0 /*|| GameObject.FindGameObjectsWithTag("Enemy1").Length <= 0*/)
            {
                Spawn();
            }
        }
    }

    void conunter()
    {

    }
    void Spawn()
    {

        StartCoroutine(Waves());

    }

    IEnumerator Waves()
    {
        waves.text = "Wave: [ 3 ]";
        yield return new WaitForSeconds(waveWait);
        for (int i = 0; i < hazardCount; i++)
        {
            int rd_num = Random.Range(0, 3);
            Vector3 spawnPosition = new Vector3(gates1[rd_num].position.x, gates1[rd_num].position.y, gates1[rd_num].position.z);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(hazard1, spawnPosition, spawnRotation);
            yield return new WaitForSeconds(spawnWait);

            //int rd_num2 = Random.Range(0, 3);
            //Vector3 spawnPosition2 = new Vector3(gates1[rd_num2].position.x, gates1[rd_num2].position.y, gates1[rd_num2].position.z);
            //Quaternion spawnRotation2 = Quaternion.identity;
            //Instantiate(hazard2, spawnPosition2, spawnRotation2);
            //yield return new WaitForSeconds(spawnWait);
        }
    }

    IEnumerator SpawnWaves()
    {
        waves.text = "Wave: [ " + waveNum.ToString() + " ]";
        waves.text = "Wave: [ 2 ]";
        yield return new WaitForSeconds(startWait);

        while (GameObject.FindGameObjectsWithTag("Enemy").Length < LIMIT || inif == true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                int rd_num2 = Random.Range(0, 3);
                Vector3 spawnPosition2 = new Vector3(gates2[rd_num2].position.x, gates2[rd_num2].position.y, gates2[rd_num2].position.z);
                Quaternion spawnRotation2 = Quaternion.identity;
                Instantiate(hazard2, spawnPosition2, spawnRotation2);
                yield return new WaitForSeconds(spawnWait);

                int rd_num1 = Random.Range(0, 3);
                Vector3 spawnPosition1 = new Vector3(gates2[rd_num1].position.x, gates2[rd_num1].position.y, gates2[rd_num1].position.z);
                Quaternion spawnRotation1 = Quaternion.identity;
                Instantiate(hazard1, spawnPosition1, spawnRotation1);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

        }

    }

    //IEnumerator SpawnWaves()
    //{
    //    waves.text = "Wave: [ " + waveNum.ToString() + " ]";
    //    while (waveNum < 9999999)
    //    {
    //        Debug.Log(waveNum.ToString());

    //        while (h1lim < hazardCount1)
    //        {
    //            //This code is intended to spawn two different enemies, but it may still be a work in progress.

    //            int rd_num = Random.Range(0, 3);
    //            Vector3 spawnPosition = new Vector3(gates1[rd_num].position.x, gates1[rd_num].position.y, gates1[rd_num].position.z);
    //            Quaternion spawnRotation = Quaternion.identity;
    //            Instantiate(hazard1, spawnPosition, spawnRotation);
    //            h1lim++;
    //            yield return new WaitForSeconds(2);                
    //        }
    //        while (h2lim < hazardCount2)
    //        {
    //            int rd_num2 = Random.Range(0, 3);
    //            Vector3 spawnPosition2 = new Vector3(gates2[rd_num2].position.x, gates2[rd_num2].position.y, gates2[rd_num2].position.z);
    //            Quaternion spawnRotation2 = Quaternion.identity;
    //            Instantiate(hazard2, spawnPosition2, spawnRotation2);
    //            h2lim++;
    //            yield return new WaitForSeconds(2);
    //        }           
    //        current = GameObject.FindGameObjectsWithTag("Enemy").Length + GameObject.FindGameObjectsWithTag("Enemy1").Length;
    //        if (current == 0)
    //        {
    //            if (hazardCount1 < 20)
    //            {
    //                hazardCount1++;
    //            }
    //            if (hazardCount2 < 20)
    //            {
    //                hazardCount2 = hazardCount2 + 2;
    //            }
    //            h1lim = 0;
    //            h2lim = 0;
    //            waveNum++;
    //            yield return new WaitForSeconds(5);
    //        }           
    //    }

    //}
}