﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {
   
    public GameObject shotSpawn;
    public Rigidbody2D rb2d;
    public float speed;

    //script for bullets
    void Start()
    {
        shotSpawn = GameObject.FindGameObjectWithTag("Enemy1");
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.AddRelativeForce(new Vector2(0, 1) * speed, ForceMode2D.Impulse);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy" || other.tag == "Enemy1")
        {
            Destroy(gameObject); //dissapear bullet
            //make enemy flash yellow when hit

            if (other.tag == "Enemy")
            {
                if (other.gameObject.GetComponent<SpriteRenderer>().color != other.gameObject.GetComponent<EnemyController>().oldColor)
                {
                    other.gameObject.GetComponent<EnemyController>().setHurtTime(2);
                }
            }
            else
            {
                if (other.gameObject.GetComponent<SpriteRenderer>().color != other.gameObject.GetComponent<EnemyControllerShooter>().oldColor)
                {
                    other.gameObject.GetComponent<EnemyControllerShooter>().setHurtTime(2);
                }
            }
            
            other.GetComponent<SpriteRenderer>().color = Color.yellow;
            //damage player based on the weapon damage of the weapon used by the player
            other.gameObject.GetComponent<EnemyHealth>().DamageEnemy(shotSpawn.GetComponent<EnemyWeapon>().getDamage());
        }
        else if (other.tag == "Arena") //bullets disappear if they hit the arena walls
        {
            Destroy(gameObject);
        }
        else if (other.tag == "Drop") //bullets disappear if they hit a drop item
        {
            Destroy(gameObject);
        }
        else if (other.tag == "Player")
        {

            other.gameObject.GetComponent<PlayerHealth>().DamagePlayer(shotSpawn.GetComponent<EnemyWeapon>().getDamage());
            other.gameObject.GetComponent<PlayerController>().makeHurt();
            Destroy(gameObject);
        }
        else
        {
            return;
        }
    }
}
