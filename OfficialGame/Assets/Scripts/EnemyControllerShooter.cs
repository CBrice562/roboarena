﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControllerShooter : MonoBehaviour {

    public int damageToPlayer; 
    public Transform target;  
    public Color oldColor;      //the original color of the bot
    public int wp_ammo;         //the ammo a weapon will have when picked up by the player
    public string gunPic;       //icon of the weapon maybe dropped by the enemy
    public string weapon;       //weapon dropped by the enemy

    private int randomWp;       //random int to determine what weapon an enemy is spawned with.
    private float range;         
    private float minDistance = 1;
    private float speed = 3;
    private float hurt_time = 2; //hurt_time is for if the enemy is hit twice, they'll flash again only if finished flashing from the first hit.
    private EnemyWeapon enemyWP;

    void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
        oldColor = GetComponent<SpriteRenderer>().color;
        enemyWP = GetComponent<EnemyWeapon>();

        randomWp = Random.Range(1, 101); //random 1-100
        setWp(randomWp);

        minDistance = 10;
        InvokeRepeating("attack", 0.5f, enemyWP.fireRate);
    }
    //The code so the enemies follow the player.
    void Update()
    {
        //rotation
        Vector3 vectorToTarget = target.position - transform.position;
        float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
        Quaternion q = Quaternion.AngleAxis(angle - 90, Vector3.forward);

        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * speed);
        //end here

        range = Vector2.Distance(transform.position, target.position);

        hurt_time -= Time.deltaTime;
        if (hurt_time < 0)
        {
            minDistance = 10;
            GetComponent<SpriteRenderer>().color = oldColor;
            hurt_time = 2;
        }

        //if (range > minDistance)
        if (range > minDistance)
        {
            GetComponent<Rigidbody2D>().mass = 3;
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        else
        {
            GetComponent<Rigidbody2D>().mass = 1;
        }
    }
    #region "Set Methods"
    //function to determine the weapon yeilded by the enemy. I made melee weapons just a little more likely than guns.
    //also, the weapon damages are the same for both the enemies and player.
    //Lastly, I made it so the player never picks up an empty gun. The random int goes from a low but not 0 num, to the max ammo.
    public void setWp(int randomWp)
    {
        if (randomWp > 0 && randomWp <= 50)
        {
            weapon = "assault";
            damageToPlayer = 20;
            gunPic = "AssaultDrop";
            wp_ammo = Random.Range(150, 301);
            enemyWP.setAssaultGun();
        }
        else if (randomWp > 50 && randomWp <= 75)
        {
            weapon = "tesla";
            damageToPlayer = 40;
            gunPic = "TeslaCoilDrop";
            wp_ammo = Random.Range(50, 131);
            enemyWP.setTeslaCoil();
        }
        else if (randomWp > 75 && randomWp <= 100)
        {
            weapon = "ray";
            damageToPlayer = 10;
            gunPic = "RaygunDrop";
            wp_ammo = Random.Range(120, 251);
            enemyWP.setRayGun();
        }
    }
    public void setMinDistance(float knockBack)
    {
        this.minDistance = knockBack;
    }
    public void setHurtTime(float hurtTime)
    {
        this.hurt_time = hurtTime;
    }
    #endregion

    private void attack()
    {
        if (weapon == "assault")
        {
            enemyWP.fireAssaultGun();
        }
        else if (weapon == "ray")
        {
            enemyWP.fireRayGun();
        }
        else if (weapon == "tesla")
        {
            enemyWP.fireTeslaCoil();
        }
    }
}
