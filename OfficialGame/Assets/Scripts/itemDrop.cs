﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class itemDrop : MonoBehaviour {
   
    public SpriteRenderer dropPic; //pic of item drop   

    private int randomDrop;
    private int time;
    private int b_ammo;
    private int addHlth;
    private string Gun;   
    private string drop;            //what drop is it?
    private GameObject thisEnemy;

    void Start()
    {             
        drop = "";
        addHlth = 0;
        time = 0;
        dropPic = gameObject.GetComponent<SpriteRenderer>();
        //random int for choosing what is dropped.
        randomDrop = Random.Range(1, 101);
        ItemDrop(randomDrop);
        //InvManager = GameObject.FindGameObjectWithTag("InventoryManager");

        if (drop == "") //if there is no drop, destroy this item
        {
            Destroy(gameObject); 
        }
    }
    void Update()
    {
        time++;
        if (time == 600) //drop dissapears after some time
        {
            Destroy(gameObject);
        }
    }
    /* So how does the item drop works. Firstly, the enemyController decides what weapon an enemy has via the setWp method.
     * itemDrop independently decides what the drop becomes via ItemDrop. 1/2 chance of no drop, and 25% chance of weapon drop.
     * Upon an enemy's death, in EnemyHealth, itemDrop's function, setWeaponDrop gets what weapon the enemy had, so that the   
     * info is transferred to itemDrop. So if the randomDrop decides that drop is "dropWeapon", the itemDrop becomes the 
     * weapon that the enemy held. Then, when the player collides with it, they pickup the weapon with the setGun function.
     */
    #region "Setting Item Drop Methods"
    public void ItemDrop(int randomDrop)
    {
        if (randomDrop > 50 && randomDrop <= 65) //15% chance of health
        {
            drop = "health";
            addHlth = 50;
            setDropPic("healthCanister");
        }
        if (randomDrop > 65 && randomDrop <= 75) //10% chance of shield
        {
            drop = "shield";
            addHlth = 100;
            setDropPic("shieldCanister");
        }
        if (randomDrop > 75 && randomDrop <= 100) //25% chance enemy drops weapon
        {
            drop = "dropWeapon";
        }
    }
    //this function gets what gun the enemy had, in case the enemy drops it.
    public void setWeaponDrop(string weapon, string gunPic, int ammo)
    {
        Gun = weapon;
        b_ammo = ammo;
        setDropPic(gunPic);
    }
    //sets pic of item drop.
    public void setDropPic(string newBoostPic)
    {
        Sprite newSprite = Resources.Load<Sprite>(newBoostPic);
        dropPic.sprite = newSprite;
    }
    #endregion
    #region "Picking Up Drop Method"
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            switch(drop)
            {
                case "health":
                    //if player just picked up health/shield, than the'll just stay that color longer
                    if (other.gameObject.GetComponent<SpriteRenderer>().color != Color.white)
                    {
                        other.gameObject.GetComponent<PlayerController>().hurt_time = 1;
                    }
                    //change color of robo during pickup
                    other.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7382F, 0.5514F, 0.7641F);
                    other.gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7382F, 0.5514F, 0.7641F);
                    other.gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.7382F, 0.5514F, 0.7641F);
                    other.gameObject.GetComponent<PlayerHealth>().addHealth(addHlth);                   
                    break;
                case "shield":
                    if (other.gameObject.GetComponent<SpriteRenderer>().color != Color.white)
                    {
                        other.gameObject.GetComponent<PlayerController>().hurt_time = 1;
                    }
                    other.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.8396F, 0.8198F, 0.8198F);
                    other.gameObject.transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.8396F, 0.8198F, 0.8198F);
                    other.gameObject.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.8396F, 0.8198F, 0.8198F);
                    other.gameObject.GetComponent<PlayerHealth>().addShield(addHlth);
                    break;
                case "dropWeapon":
                    //currently, I have it so the player chooses to currently holds weapon it runs over. Will change soon.
                    other.gameObject.GetComponent<PlayerController>().setGun(Gun, b_ammo);
                    other.gameObject.GetComponent<InventoryManager>().AddItem(other.gameObject.GetComponent<Weapon>().wp_ID, b_ammo);
                    break;
                default:
                    break;
            }
            Destroy(gameObject); //destroy item after it's picked up
        }
    }
    #endregion
}
