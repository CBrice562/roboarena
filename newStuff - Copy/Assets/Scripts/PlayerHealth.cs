﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {

    public int playerMaxHealth;
    public int playerCurrentHealth;

    public float waitToReload;
    private bool reloading;
    private GameObject thePlayer;

	// Use this for initialization
	void Start () {
        waitToReload = 2;
        playerCurrentHealth = playerMaxHealth;
        thePlayer = GameObject.FindWithTag("Player");
    }
	
	// Update is called once per frame
	void Update () {
		
        if (playerCurrentHealth <= 0)
        {
            thePlayer.SetActive(false);
            reloading = true;
            SceneManager.LoadScene("main");
        }

        if (reloading == true)
        {
            waitToReload -= Time.deltaTime;
            if (waitToReload < 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                thePlayer.SetActive(true);
            }           
        }
	}

    public void DamagePlayer(int damage)
    {
        playerCurrentHealth -= damage;
    }

    public void SetMaxHealth()
    {
        playerCurrentHealth = playerMaxHealth;
    }
}
