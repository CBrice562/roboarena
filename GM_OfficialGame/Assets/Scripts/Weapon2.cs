﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon1 : MonoBehaviour {

    public float fireRate;  
    public Transform shotSpawn;       //the shot spawns from the player
    public GameObject shot;           //the bullet itself
    public GameObject shotPic;        //pic of the bullet
    public AudioSource actionSource;  //source of weapon sounds
    public AudioClip attack;          //weapon sound clip
    public float ShotingPositionleft = -1.9f;
    public float ShotingPositionright = -0.5f;
    public int wp_damage;
    public int wp_ammo;

    private float nextFire;

    void Start()
    {
        actionSource = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        actionSource.volume = 0.05f;       
        actionSource.clip = attack;
    }
    public int getAmmo()
    {
        return wp_ammo;
    }
    //all of these set methods are just setting the settings for the weapon the player is currently using.
    //each weapon has a different damage, fire rate, max ammo, etc 
    public void setAssaultGun(int ammo)
    {
        attack = Resources.Load("Sounds/assault", typeof(AudioClip)) as AudioClip;       
        Sprite newSprite = Resources.Load<Sprite>("DiamondAssault");
        shotPic.GetComponent<SpriteRenderer>().sprite = newSprite;
        shot.GetComponent<Bullet>().speed = 10;
        ShotingPositionleft = -1.9f;
        ShotingPositionright = -0.5f;
        wp_damage = 15; 
        nextFire = 0.9f;
        wp_ammo = ammo;
    }
    public void setRayGun(int ammo)
    {
        attack = Resources.Load("Sounds/ray", typeof(AudioClip)) as AudioClip;
        Sprite newSprite = Resources.Load<Sprite>("DiamondRay");
        shotPic.GetComponent<SpriteRenderer>().sprite = newSprite;
        shot.GetComponent<Bullet>().speed = 5;
        ShotingPositionleft = -1.18f;
        ShotingPositionright = -0f;
        wp_damage = 10;
        nextFire = 0f;
        wp_ammo = ammo;
    }
    public void setTeslaCoil(int ammo)
    {
        attack = Resources.Load("Sounds/tesla", typeof(AudioClip)) as AudioClip;
        Sprite newSprite = Resources.Load<Sprite>("DiamondTesla");
        shotPic.GetComponent<SpriteRenderer>().sprite = newSprite;
        shot.GetComponent<Bullet>().speed = 8;
        ShotingPositionleft = -1.18f;
        ShotingPositionright = -0f;
        wp_damage = 40;
        nextFire = 0.1f;
        wp_ammo = ammo;
    }
    public void setDeathStick(int ammo)
    {       
        attack = Resources.Load("Sounds/stick", typeof(AudioClip)) as AudioClip;
    }
    public void setDrill(int ammo)
    {       
        attack = Resources.Load("Sounds/drill", typeof(AudioClip)) as AudioClip;
    }
    public void setBoxing()
    {

    }

    //all of these methods are just the attacking methods of the weapons.
    public void assaultGun()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Time.time*1.3 > nextFire && (wp_ammo > 0))
        {           
            Vector3 a, b;
            Vector3 left = new Vector3(ShotingPositionleft, 0, 0);
            Vector3 right = new Vector3(ShotingPositionright, 0, 0);
            a = shotSpawn.position + left;
            b = shotSpawn.position + right;

            nextFire = Time.time + fireRate;
            Instantiate(shot, a, shotSpawn.rotation);
            Instantiate(shot, b, shotSpawn.rotation);
            wp_ammo -= 2;
            if (wp_ammo < 0)
            {
                wp_ammo = 0;
            }
            if (actionSource.isPlaying == false)
            {
                attack.LoadAudioData();
                actionSource.clip = attack;
                actionSource.Play();
            }
        }      
    }
    public void rayGun()
    {
        //The ray gun can be fired via holding down the space bar
        if (Input.GetKey(KeyCode.Space) && (wp_ammo > 0))
        {           
            Vector3 a;
            Vector3 left = new Vector3(ShotingPositionleft, 0, 0);
            a = shotSpawn.position + left;

            nextFire = Time.time + fireRate;
            Instantiate(shot, a, shotSpawn.rotation);
            wp_ammo -= 1;

            if (actionSource.isPlaying == false)
            {
                attack.LoadAudioData();
                actionSource.clip = attack;
                actionSource.Play();
            }
        }
        else
        {
            actionSource.Stop();
        }
    }
    public void teslaCoil()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Time.time > nextFire && (wp_ammo > 0))
        {
            attack.LoadAudioData();
            actionSource.clip = attack;
            actionSource.Play();

            Vector3 a;
            Vector3 left = new Vector3(ShotingPositionleft, 0, 0);
            a = shotSpawn.position + left;

            nextFire = Time.time + fireRate;
            Instantiate(shot, a, shotSpawn.rotation);
            wp_ammo -= 1;
        }
    }
    public void deathStick()
    {

    }
    public void drill()
    {

    }
    public void boxing()
    {

    }
}
