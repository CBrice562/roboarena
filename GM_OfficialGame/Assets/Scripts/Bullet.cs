﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float speed;
    Rigidbody2D rb2d;

    //script for bullets
    void Start()
    {   
        speed = 12;
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.AddRelativeForce(new Vector2(0, 1) * speed, ForceMode2D.Impulse);
        //Debug.Log(shotSpawn.transform.forward);
    }
}
