﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {

    public string shooter;

    void OnTriggerEnter2D(Collider2D other)
    {

        //Debug.Log("Called1111");
        if (other.tag == "Enemy" && shooter == "Player")
        {
            //Debug.Log("Called for sucide" + GetComponent<Weapon>().shotSpawn.tag);
            Destroy(gameObject); //dissapear bullet
            //make enemy flash yellow when hit

            if (other.gameObject.GetComponent<SpriteRenderer>().color != other.gameObject.GetComponent<EnemyController>().oldColor)
            {
                other.gameObject.GetComponent<EnemyController>().hurt_time = 2;
            }
            other.GetComponent<SpriteRenderer>().color = Color.yellow;
            //damage player based on the weapon damage of the weapon used by the player
            //Debug.Log("Called");
            //Debug.Log(GameObject.FindWithTag("Player").GetComponent<Weapon>().GetDamege() + "  Demage");
            other.gameObject.GetComponent<EnemyHealth>().DamageEnemy(GameObject.FindWithTag("Player").GetComponent<Weapon>().GetDamege());
        } else if (other.tag == "Player" && shooter == "Enemy") {
            Destroy(gameObject);
            //Debug.Log("Called for sucide" + GetComponent<Weapon>().shotSpawn.tag);
            //damage player based on the weapon damage of the weapon used by the player
            //Debug.Log("Called");
            //Debug.Log(GameObject.FindWithTag("Player").GetComponent<Weapon>().GetDamege() + "  Demage");
            other.gameObject.GetComponent<PlayerHealth>().DamagePlayer(GameObject.FindWithTag("Enemy").GetComponent<Weapon>().GetDamege());
        }
        else if (other.tag == "Arena") //bullets disappear if they hit the arena walls
        {
            Destroy(gameObject);
        }
        else if (other.tag == "Boost") //bullets disappear if they hit a drop item
        {
            Destroy(gameObject);
        }
        else
        {
            return;
        }
    }
}
