﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject hazard1;
    public GameObject hazard2;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public int LIMIT = 10;
    public bool inif = true;
    public struct Gate
    {
        public Vector3 position;
        public Gate(float x, float y, float z)
        {
            position.x = x;
            position.y = y;
            position.z = z;
        }
    }
    Gate[] gates1 = new Gate[]{
        new Gate(-10, 8, 0),
        new Gate(10, 8, 0),
        new Gate(10, -8, 0),
        new Gate(-10, -8, 0)
    };
    Gate[] gates2 = new Gate[]{
        new Gate(10, -8, 0),
        new Gate(-10, -8, 0),
        new Gate(-10, 8, 0),
        new Gate(10, 8, 0)
    };
    //I believe the above Gates are spawning coordinates for the enemies. I can't say much though since guanqing wrote this portion.
    void Start()
    {
        //StartCoroutine(SpawnWaves());
        //Spawn();
        StartCoroutine(SpawnWaves());
    }

    /*
    private void Update()
    {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 0) {
            Spawn();
        }

    }
    */


    void wiatSeconds()
    {
        while (true) {
            if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 0)
            {
                Spawn();
            }
        }
    }




    void conunter() {




    }



    void Spawn() {

        StartCoroutine(Waves());

    }

    IEnumerator Waves() {
        yield return new WaitForSeconds(waveWait);
        for (int i = 0; i < hazardCount; i++)
        {
            int rd_num = Random.Range(0, 4);
            Vector3 spawnPosition = new Vector3(gates1[rd_num].position.x, gates1[rd_num].position.y, gates1[rd_num].position.z);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(hazard1, spawnPosition, spawnRotation);
            yield return new WaitForSeconds(spawnWait);
        }
    }

    
    /*
    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (GameObject.FindGameObjectsWithTag("Enemy").Length < LIMIT || inif == true)
        {
            //This code is intended to spawn two different enemies, but it may still be a work in progress.
            for (int i = 0; i < hazardCount; i++)
            {
                int rd_num = Random.Range(0, 4);
                Vector3 spawnPosition = new Vector3(gates1[rd_num].position.x, gates1[rd_num].position.y, gates1[rd_num].position.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard1, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            /*
            for (int i = 0; i < hazardCount; i++)
            {
                int rd_num = Random.Range(0, 4);
                Vector3 spawnPosition = new Vector3(gates2[rd_num].position.x, gates2[rd_num].position.y, gates2[rd_num].position.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard2, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            */
            yield return new WaitForSeconds(waveWait);
        }
    }
    */
}