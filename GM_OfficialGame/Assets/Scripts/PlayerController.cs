﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;   
    public Image weaponPic;  
    public Text weaponTxt;
    public Text ammoText;
    public Transform head;  
    public Transform arms;   
    public AudioClip walk;   //robo walking sound
    public AudioClip action; //for sound when robo gets hit
    public AudioSource musicSource; //plays walking
    public AudioSource actionSource;//plays robo geting hit
    public float hurt_time = 1;     //tells how long robo changes color after being hurt (if he gets hit twice in a row, he stays red longer)
    public string pweapon;          //string to hold player current weapon

    private bool playerMoving;
    private Vector2 lastMove;
    private Vector2 lastDir;
    private Animator anim;
    private Rigidbody2D myRigidbody;
    private PlayerHealth pHealth;   //player health script
    private Weapon wp;
    private Quaternion rtate;
    private bool movingDown;
    private bool setDefault;
    private string armSprite;

    void Start() {
        anim = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody2D>();
        pHealth = GetComponent<PlayerHealth>();       
        wp = GetComponent<MyRay>();
        musicSource = GetComponent<AudioSource>();                      
        head = this.gameObject.transform.GetChild(0);
        arms = this.gameObject.transform.GetChild(1);
        musicSource.clip = walk;
        actionSource.clip = action;

        pweapon = wp.weaponname;
        setGun(250);
    }
   
    void Update() {

        moveSpeed = 9;
        playerMoving = false;       
        hurt_time -= Time.deltaTime;
        //Debug.Log("Called");
        rangeAttack(); //shoot weapon

        if (hurt_time < 0) //when robo finish being hurt, go back to normal color
        {
            GetComponent<SpriteRenderer>().color = Color.white;
            head.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            arms.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            hurt_time = 1;
        }
        //this if statement makes the speed moving diagonal equivalent to the speed moving regularly
        if (System.Math.Abs(Input.GetAxisRaw("Horizontal")) > 0.5f && System.Math.Abs(Input.GetAxisRaw("Vertical")) > 0.5f)
        {
            moveSpeed = 6;
        }
        //code to move player down
        if (System.Math.Abs(Input.GetAxisRaw("Horizontal")) == 0f && Input.GetAxisRaw("Vertical") < -0.5f)
        {
            rtate = Quaternion.Euler(0, 0, 180);
            movingDown = true;
        }
        else if (System.Math.Abs(Input.GetAxisRaw("Horizontal")) > 0.5f || Input.GetAxisRaw("Vertical") > 0.5f)
        {
            movingDown = false;
        }
        //if (Input.GetAxisRaw("Horizontal") > 0.5f && Input.GetAxisRaw("Vertical") > 0.5f)
        //{
        //    rtate = Quaternion.Euler(0, 0, -45);
        //}

        lastMove = new Vector2(0f, 0f);
        //code to move player in other directions
        if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)
        {
            playerMoving = true;
            lastMove.x = Input.GetAxisRaw("Horizontal");
        }
        if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f)
        {
            playerMoving = true;
            lastMove.y = Input.GetAxisRaw("Vertical");
        }

        myRigidbody.velocity = new Vector2(lastMove.x * moveSpeed, lastMove.y * moveSpeed);

        anim.SetFloat("MoveX", Input.GetAxisRaw("Horizontal"));
        anim.SetFloat("MoveY", Input.GetAxisRaw("Vertical"));
        anim.SetBool("PlayerMoving", playerMoving);
        anim.SetFloat("LastMoveX", lastMove.x);
        anim.SetFloat("LastMoveY", lastMove.y);

        if (!playerMoving)
        {
            if (movingDown == true)
            {
                transform.rotation = rtate;
            }
            else
            {
                transform.up = lastDir;
            }
        }
        else
        {
            if (musicSource.isPlaying == false) //robo walking will play only after current loop is done
            {
                musicSource.Play();
            }
            lastDir = myRigidbody.velocity;

            if (movingDown == true)
            {
                transform.rotation = rtate;
            }
            else
            {
                transform.up = lastDir;
            }
        }
    }
    //this function will be called whenever player chooses a new weapon to currently yeild.
    //the preferences and settings will be set in the weapon script. 
    //string weapon determines the weapon and ammo is just to keep track of how much that weapon has.
    public void setGun(int ammo)
    {   
            wp.Set(ammo);
            weaponTxt.text = wp.getName();
            ammoText.text = "Fuel: " + wp.GetAmmo().ToString() + "/" + wp.wp_cap.ToString();
            pweapon = wp.getName();

    }
    //this function is an active weapon attack
    public void rangeAttack()
    {
        //Debug.Log("Called");
        wp.PlayerAttack();
        ammoText.text = "  Ammo: " + wp.GetAmmo().ToString() + "/" + wp.wp_cap.ToString(); ;      
    }
    //this function is to set the picture in the box in the upper right corner. The picture will be the current weapon.
    public void setPic(string newWeaponPic)
    {
        byte[] picData;
        picData = File.ReadAllBytes(newWeaponPic);

        Texture2D newTexture = new Texture2D(55, 38);
        newTexture.LoadImage(picData);

        weaponPic.sprite = Sprite.Create(newTexture, new Rect(0, 0, 55, 38), new Vector2(0.5f, 0.5f), weaponPic.sprite.pixelsPerUnit);
    }

    //This function checks if an enemy is hitting the player. Will be edited to check if bullets or melee are hitting the player.
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            //for sound of player getting hurt
            action = Resources.Load("Sounds/robotHurt", typeof(AudioClip)) as AudioClip;
            action.LoadAudioData();
            actionSource.clip = action;
            if (GetComponent<SpriteRenderer>().color != Color.white)
            {
                hurt_time = 1; 
            }            
            actionSource.Play();
            //make color of bot hurt
            GetComponent<SpriteRenderer>().color = new Color(0.9905F, 0.7522F, 0.7522F);
            head.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.9905F, 0.7522F, 0.7522F);
            arms.gameObject.GetComponent<SpriteRenderer>().color = new Color(0.9905F, 0.7522F, 0.7522F);
            //use the health script to take health from the player.
            pHealth.DamagePlayer(other.gameObject.GetComponent<EnemyController>().damageToPlayer);           
        }  
    }
}
