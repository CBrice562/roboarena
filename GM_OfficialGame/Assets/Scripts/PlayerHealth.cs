﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public int playerHealth;
    public int playerShield;
    public int playerScore;
    public Text healthText;
    public Text scoreText;
    public Text shieldText;
    public float waitToReload; //this float is just so there is a bit of a delay after death before respawning.

    private bool reloading;
    private GameObject thePlayer;

	void Start () {
        waitToReload = 2;
        playerHealth = 200 ;
        setHealthText(playerHealth);
        playerShield = 200;
        setShieldText(playerShield);
        thePlayer = this.gameObject;
    }
	
	void Update () {
		
        if (playerHealth <= 0)
        {
            thePlayer.SetActive(false);
            reloading = true;
            SceneManager.LoadScene("main");
        }
        //This code is a work in progress
        //if (reloading == true)
        //{
        //    waitToReload -= Time.deltaTime;
        //    if (waitToReload < 0)
        //    {
        //        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //        thePlayer.SetActive(true);
        //    }           
        //}
	}
    //I think the text setting methods are pretty self explanatory
    public void setHealthText(int newHealth)
    {
        if (newHealth > 0)
        {
            healthText.text = "Health: " + newHealth.ToString() + "/200";
        }
        else
        {
            healthText.text = "Powered Off";
        }
    }
    public void setShieldText(int newShield)
    {
        if (newShield > 0)
        {
            shieldText.text = "Shield: " + newShield.ToString() + "/200";
        }
        else 
        {
            shieldText.text = "Shield Down";
        }

    }
    public void setScoreText(int newScore)
    {
        if (newScore < 10)
        {
            scoreText.text = "Score: 000000" + newScore.ToString();
        }
        else if (newScore < 100)
        {
            scoreText.text = "Score: 00000" + newScore.ToString();
        }
        else if (newScore < 1000)
        {
            scoreText.text = "Score: 0000" + newScore.ToString();
        }
        else if (newScore < 10000)
        {
            scoreText.text = "Score: 000" + newScore.ToString();
        }
        else if (newScore < 100000)
        {
            scoreText.text = "Score: 00" + newScore.ToString();
        }
        else if (newScore < 1000000)
        {
            scoreText.text = "Score: 0" + newScore.ToString();
        }
        else
        {
            scoreText.text = "Score: " + newScore.ToString();
        }
    }
    public void DamagePlayer(int damage) 
    {
        if (playerShield > 0) //if theres a shield, damage the shield
        {
            playerShield -= damage;

            if (playerShield < 0) //shield/health should never go below 0.
            {                                
                playerShield = 0;
            }
            setShieldText(playerShield);            
        }
        else //shield is down
        {
            playerHealth -= damage;

            if (playerHealth < 0)
            {
                playerHealth = 0;               
            }
            setHealthText(playerHealth);
        }
    }
    public void addHealth(int hUp)
    {
        playerHealth += hUp;

        if (playerHealth > 200) //health/shield should not go over 200.
        {
            playerHealth = 200;
        }
        setHealthText(playerHealth);
    }
    public void addShield(int sUp)
    {
        playerShield += sUp;

        if (playerShield > 200)
        {
            playerShield = 200;           
        }
        setShieldText(playerShield);
    }
    public void addScore(int scoreUp)
    {
        playerScore += (scoreUp * 10); //score is accumulating via the health of the enemy * 10.

        if (playerScore > 9999999)
        {
            playerScore = 9999999;           
        }
        setScoreText(playerScore);
    }
}
